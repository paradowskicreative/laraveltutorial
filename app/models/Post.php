<?php

use Illuminate\Support\Facades\Validator;

class Post extends Eloquent{

    protected $table = 'posts';
    public $timestamps = false;

    protected $fillable = [
        'title',
        'content'
    ];

    static public function validate($inputs)
    {
        $rules = [
            'title' => 'required'
        ];

        return Validator::make($inputs, $rules);
    }
}