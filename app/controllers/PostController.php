<?php

class PostController extends BaseController {


    public function index(){

        $rows = Post::all();

        return View::make('posts.index', ['posts' => $rows]);
    }

    public function store()
    {
        $inputs = Input::all();
        if ( Post::validate($inputs)->fails() )
        {
            return 'error';
        }else{
            $post = Post::create($inputs);
        }

//        $post = new Post;
//        $post->title   = Input::get('title');
//        $post->content = Input::get('content');
//        $post->save();

        return $post->id;
    }

    public function update($id)
    {
        $post = Post::find($id);
        $post->title = Input::get('title');
        $post->content = Input::get('content');
        $post->save();
        return 'Saved!';
    }
}